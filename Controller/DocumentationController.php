<?php
declare(strict_types=1);

namespace MicroDoc\Controller;

use Microframe\Mvc\Controller\Controller;
use MicroDoc\Component\DocumentationComponent;

class DocumentationController extends Controller
{
    private DocumentationComponent $component;

    public function initialize()
    {
        $this->component = new DocumentationComponent();
    }

    public function indexAction()
    {
        $this->view->pick("folderList");
        $this->view->folderList = $this->component->getFrameFolderList();
    }

    public function listFolderFilesAction()
    {
        $this->view->pick("fileList");
        $this->view->fileList = $this->component->getFrameFilelist($this->request->get("folder"));
    }

    public function fileAction()
    {
        $this->view->description = $this->component->reflectClass($this->request->get("namespace"));
        $this->view->pick("fileDescription");
    }
}