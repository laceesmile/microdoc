<?php

declare(strict_types=1);

namespace MicroDoc;

use Microframe\Router\Router;
use Microframe\Router\RoutesInterface;

class Routes implements RoutesInterface
{
    public function initialize(Router $router, string $module): Router
    {
        $router->addGet("/microdoc/documentor", [
            "module" => $module,
            "controller" => "documentation",
            "action" => "index",
        ]);

        $router->addGet("/microdoc/documentor/folder", [
            "module" => $module,
            "controller" => "documentation",
            "action" => "listFolderFiles",
            "name" => "microdoc_documentor_folder_filelist",
        ]);

        $router->addGet("/microdoc/documentor/file", [
            "module" => $module,
            "controller" => "documentation",
            "action" => "file",
            "name" => "microdoc_documentor_file",
        ]);
        
        
        return $router;
    }
}