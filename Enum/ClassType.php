<?php

declare(strict_types=1);

namespace MicroDoc\Enum;

use Microframe\Helper\Getter;

class ClassType
{    
    public const TYPE_INTERFACE = "interface";

    public const TYPE_ABSTRACT = "abstract";

    public const TYPE_CLASS = "class";

    public const TYPE_FINAL = "final";

    public const TYPE_TRAIT = "trait";

    public const TYPE_ANONYMUS = "anonymus";

    private const TYPE_TO_TOKEN = [
        T_INTERFACE => self::TYPE_INTERFACE,
        T_ABSTRACT => self::TYPE_ABSTRACT,
        T_CLASS => self::TYPE_CLASS,
        T_FINAL => self::TYPE_FINAL,
        T_TRAIT => self::TYPE_TRAIT,
    ];

    public static function getByToken(int $typeConst)
    {
        return self::TYPE_TO_TOKEN[$typeConst] ?? null;
    }


}