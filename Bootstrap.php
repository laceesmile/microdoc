<?php

declare(strict_types=1);

namespace MicroDoc;

use Microframe\Mvc\View;
use Microframe\Router\Router;
use Microframe\Factory\Application;
use Microframe\Factory\ClassBuilder;
use Microframe\Factory\FactoryDefaults;

class Bootstrap
{
    public function __construct()
    {
        $router = new Router();

        $di = (new FactoryDefaults())->getDI();
        
        $router->parseRoutes([
            "MicroDoc\\Routes",
        ]);
        
        $app = new Application();
        
        $microDoc = $router->getRouteByUrl($di->request->getUrl()->getUrl(), $di->request->getMethod());
        if(!$microDoc) {
            return;
        }
        
        $app->set("router", $router);

        if(!$app->hasService("view"))
        $app->set("view", (new View(
            (new ClassBuilder())
                ->set("assetsPath", MICRODOC_BASE_PATH . "View" . DIR_SEP . "Assets")
            ))
            ->setViewPath(MICRODOC_BASE_PATH . "View")
            ->setLayout(MICRODOC_BASE_PATH . "Layout" . DIRECTORY_SEPARATOR . "layout")
        );

        $app->assets->innerCss("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css", false);
        $app->assets->innerCss("microdoc");

        try {
            $app->run();
            exit(1);
        } catch (\Throwable $e) {
            return;
        }
    }
}