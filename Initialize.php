<?php

declare(strict_types=1);


if(!function_exists("print_pre")) {
    function print_pre() {
        foreach(func_get_args() as $printable) {
            echo "<pre>";
            print_r($printable);
            echo "<pre>";
        }
    }
}

define("DIR_SEP", DIRECTORY_SEPARATOR);
define("MICRODOC_BASE_PATH", dirname(__FILE__) . DIR_SEP);
$microDoc = new MicroDoc\Bootstrap();