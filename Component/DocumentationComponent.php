<?php

declare(strict_types=1);

namespace MicroDoc\Component;

use ReflectionClass;
use ReflectionMethod;
use FilesystemIterator;
use MicroDoc\Enum\ClassType;
use Microframe\Common\NativeArray;
use Microframe\Helper\Helper;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class DocumentationComponent
{
    use Helper;

    public function getFrameFilelist(?string $target = null, ?array &$fileList = []): array
    {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->getTarget($target), FilesystemIterator::SKIP_DOTS));
        foreach ($iterator as $item) {
            if (strpos($item->getPathName(), ".git")) continue;
            if ($item->isDir()) {
                $this->getFrameFilelist($item->getPathName(), $fileList);
            } elseif ($item->getExtension() === "php") {
                $namespace = $this->generateFileNamespace($item->getPathName());
                if ($namespace) {
                    $fileList[] = $namespace;
                }
            }
        }
        return $fileList;
    }

    public function getFrameFolderList(): array
    {
        return array_map(function ($item) {
            return basename($item);
        }, glob($this->getTarget() . "*", GLOB_ONLYDIR));
    }

    public function reflectClass(string $namespace): NativeArray
    {
        $classReflection = new ReflectionClass($namespace);
        $functions = $classReflection->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED);
        if(empty($functions)) {
            return new NativeArray([]);
        }
        $traits = $classReflection->getTraitNames();
        $interfaces = $classReflection->getInterfaceNames();
        $parent = $classReflection->getParentClass() ? : [];
        $file = $functions[0]->getFileName();
        $functions = array_filter($functions, function($fnc) use($namespace, $file) {
            return $fnc->class == $namespace and $file == $fnc->getFileName();
        });

        return new NativeArray([
            "traits" => $traits,
            "interfaces" => $interfaces,
            "parent" => $parent,
            "functions" => $functions,
            "description" => $classReflection->getDocComment(),
        ]);
    }

    private function getTarget(?string $target = null): string
    {
        if (!is_null($target) and file_exists($target)) {
            return $target;
        }
        return rtrim($this->constValue("MICRODOC_DOC_PATH", join(DIR_SEP, [
            $this->getVendorPath(),
            "lacee",
            "Microframe",
        ])), DIR_SEP) . DIR_SEP . $target;
    }

    private function getVendorPath(): string
    {
        return dirname((new \ReflectionClass(\Composer\Autoload\ClassLoader::class))->getFileName(), 2);
    }

    private function generateFileNamespace(string $fullPath): ?string
    {
        $definition = $this->getFileDefinitions($fullPath);
        if (empty($definition)) return null;
        return $definition["namespace"] . "\\" . $definition[$definition["type"]];
    }


    private function getFileDefinitions(string $file): array
    {
        $fp = fopen($file, 'r');
        $buffer = null;
        $definition = [];
        $buffer = stream_get_contents($fp);
        if (!strpos($buffer, "namespace")) return [];

        $tokens = token_get_all($buffer);

        for ($i = 0; $i < count($tokens); $i++) {
            if (!is_array($tokens)) continue;
            switch ($tokens[$i][0]) {
                case T_NAMESPACE: {
                        $definition["namespace"] = trim($this->get($tokens, $i, ";"));
                    }
                    break;
                case T_CLASS:
                case T_INTERFACE:
                case T_TRAIT: {
                        $definition[ClassType::getByToken($tokens[$i][0])] = trim($this->get($tokens, $i));
                        $definition["type"] = ClassType::getByToken($tokens[$i][0]);
                        fclose($fp);
                        return $definition;
                    }
                break;
            }
        }

        return $definition;
    }

    private function get(array &$tokens, int $i, string $end = " "): string
    {
        $ret = "";
        for ($j = $i + 2; $j < count($tokens); $j++) {
            if ($tokens[$j] === $end or $tokens[$j] === "{" or !is_array($tokens[$j]) or $tokens[$j][1] === " ") break;
            $token = preg_replace("/\s+\n/", "", $tokens[$j][1]);
            if (empty($token)) continue;
            $ret .= $token;
        }
        return $ret;
    }
}
